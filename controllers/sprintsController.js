const express = require('express');

// Collection reference
const Sprint = require('../models/sprint');

// List items
function index(request, response, next) {
  const page = request.params.page ? request.params.page : 1;

  Sprint.paginate({}, {
    page: page,
    limit: 10
  }, (err, objs) => {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t read the Sprints   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'There you have the Sprints list!   ( ^-^)/',
        objs: objs
      });
    }
  });
}

// Create item
function create(request, response, next) {
  let sprint = new Sprint();

  sprint.sprintName = request.body.sprintName;
  sprint.duration = {
    'startDate': request.body.startDate,
    'endDate': request.body.endDate
  };
  sprint.goal = request.body.goal;
  sprint.remainingHours = request.body.remainingHours;
  sprint.status = 'Active';
  sprint.project = request.body.project;

  sprint.save((err, obj) => {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t save the Sprint   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'Sprint saved correctly!   ╰( ^ᴗ^)╯',
        objs: obj
      });
    }
  });
}

// Update item
function update(request, response, next) {
  let sprint = {};

  sprint.sprintName = request.body.sprintName;
  sprint.duration = {
    'startDate': request.body.startDate,
    'endDate': request.body.endDate
  };
  sprint.goal = request.body.goal;
  sprint.feedback = request.body.feedback;
  sprint.status = request.body.status;
  sprint.project = request.body.project;

  Sprint.findByIdAndUpdate(request.params.id, {$set: sprint}, function (err) {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t find the Sprint   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'Sprint updated correctly!   (ﾉ ^ᴗ^)ﾉ',
        objs: sprint
      });
    }
  });
}

// Delete item
function remove(request, response, next) {
  const id = request.params.id;

  if (id) {
    Sprint.remove({
      _id: id
    }, function(err) {
      if (err) {
        response.json({
          error: true,
          message: 'Mmm... Couldn’t delete the Sprint   ( ಠ_ಠ)7',
          objs: {}
        });
      } else {
        response.json({
          error: false,
          message: 'Sprint deleted correctly!   (ﾉ ^ᴗ^)ﾉ',
          objs: {}
        });
      }
    });
  } else {
    response.json({
      error: true,
      message: 'Mmm... Couldn’t find the Sprint   ( ಠ_ಠ)7',
      objs: {}
    });
  }
}

module.exports = {
  index,
  create,
  update,
  remove
};

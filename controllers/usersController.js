const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');

// Collection reference
const User = require('../models/user');

// List users
function index(request, response, next) {
  const page = request.params.page ? request.params.page : 1;

  User.paginate({}, {
    page: page,
    limit: 10
  }, (err, objs) => {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t read the Users   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'There you have the Users list!   ( ^-^)/',
        objs: objs
      });
    }
  });
}

// Create user
function create(request, response, next) {
  let user = new User();

  user.name = request.body.name;
  user.birthdate = request.body.birthdate;
  user.email = request.body.email;
  user.curp = request.body.curp;
  user.rfc = request.body.rfc;
  user.address = request.body.address;
  user.skills = {
    'skillName': request.body.skillName,
    'experience': request.body.experience,
  };
  user.projects = {
    'project': request.body.project,
    'userType': request.body.userType,
  };

  const saltRounds = 10;
  const password = request.body.password;

  bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(password, salt, function(err, hash) {

      user.password = hash;
      user.salt = salt;

      user.save((err, obj) => {
        if (err) {
          response.json({
            error: true,
            message: 'Mmm... Couldn’t save the User   ( ಠ_ಠ)7',
            objs: {}
          });
        } else {
          obj.password = null;
          obj.salt = null;
          response.json({
            error: false,
            message: 'User saved correctly!   ╰( ^ᴗ^)╯',
            objs: obj
          });
        }
      });
    });
  });
}

// Update user
function update(request, response, next) {
  let user = {};

  user.name = request.body.name;
  user.birthdate = request.body.birthdate;
  user.email = request.body.email;
  user.curp = request.body.curp;
  user.rfc = request.body.rfc;
  user.address = request.body.address;
  user.skills = {
    'skillName': request.body.skillName,
    'experience': request.body.experience,
  };
  user.projects = {
    'project': request.body.project,
    'userType': request.body.userType,
  };

  User.findByIdAndUpdate(request.params.id, {$set: user}, function (err) {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t update the User   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'User updated correctly!   (ﾉ ^ᴗ^)ﾉ',
        objs: user
      });
    }
  });
}

// Delete user
function remove(request, response, next) {
  const id = request.params.id;

  if (id) {
    User.remove({
      _id: id
    }, function(err) {
      if (err) {
        response.json({
          error: true,
          message: 'Mmm... Couldn’t delete the User   ( ಠ_ಠ)7',
          objs: {}
        });
      } else {
        response.json({
          error: false,
          message: 'User deleted correctly!   (ﾉ ^ᴗ^)ﾉ',
          objs: {}
        });
      }
    });
  } else {
    response.json({
      error: true,
      message: 'Mmm... Couldn’t found the User   ( ಠ_ಠ)7',
      objs: {}
    });
  }
}

module.exports = {
  index,
  create,
  update,
  remove
};

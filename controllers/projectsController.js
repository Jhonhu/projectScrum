const express = require('express');

// Collection reference
const Project = require('../models/project');

// List projects
function index(request, response, next) {
  const page = request.params.page ? request.params.page : 1;

  Project.paginate({}, {
    page: page,
    limit: 10
  }, (err, objs) => {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t read the Projects   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'There you have the Projects list!   ( ^-^)/',
        objs: objs
      });
    }
  });
}

// Create project
function create(request, response, next) {
  let project = new Project();

  project.projectName = request.body.projectName;
  project.description = request.body.description;
  project.requestDate = request.body.requestDate;
  project.startDate = request.body.startDate;

  project.save((err, obj) => {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t save the Project   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'Project saved correctly!   ╰( ^ᴗ^)╯',
        objs: obj
      });
    }
  });
}

// Update project
function update(request, response, next) {
  let project = {};

  project.projectName = request.body.projectName;
  project.description = request.body.description;
  project.requestDate = request.body.requestDate;
  project.startDate = request.body.startDate;

  Project.findByIdAndUpdate(request.params.id, {$set: project}, function (err) {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t update the Project   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'Project updated correctly!   (ﾉ ^ᴗ^)ﾉ',
        objs: project
      });
    }
  });
}

// Delete project
function remove(request, response, next) {
  const id = request.params.id;

  if (id) {
    Project.remove({
      _id: id
    }, function(err) {
      if (err) {
        response.json({
          error: true,
          message: 'Mmm... Couldn’t delete the Project   ( ಠ_ಠ)7',
          objs: {}
        });
      } else {
        response.json({
          error: false,
          message: 'Project deleted correctly!   (ﾉ ^ᴗ^)ﾉ',
          objs: {}
        });
      }
    });
  } else {
    response.json({
      error: true,
      message: 'Mmm... Couldn’t find the Project   ( ಠ_ಠ)7',
      objs: {}
    });
  }
}

module.exports = {
  index,
  create,
  update,
  remove
};

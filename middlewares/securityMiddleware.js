const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('config');


function verifyToken(request, response, next) {
  const token = request.body.token || request.query.token || request.headers['x-access-token'];
  const key = config.get('api.key');
  if (token) {
    jwt.verify(token, key, (err, decoded) => {
      if (err) {
        response.json({
          error: true,
          message: 'Key not found: Access denied!   ᕙ( ಠ益ಠ)ᕗ',
          objs: {}
        });
      } else {
        next();
      }
    });
  } else {
    response.json({
      error: true,
      message: 'Key not found: Access denied!   ᕙ( ಠ益ಠ)ᕗ',
      objs: {}
    });
  }
}

module.exports = {
  verifyToken
};

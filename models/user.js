const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;

// Schema definition
const userSchema = Schema({
  name: String,
  birthdate: Date,
  email: String,
  curp: String,
  rfc: String,
  address: String,
  skills: [{
    skillName: String,
    experience: {
      type: String,
      enum: ['Junior', 'Senior', 'Master']
    }
  }],
  projects: [{
    project: {
      type: Schema.Types.ObjectId,
      ref: 'Project'
    },
    userType: {
      type: String,
      enum: ['Developer', 'Product Owner', 'Scrum Master']
    }
  }],
  password: String,
  salt: String,

  provider_id : {type: String, unique: true}, // ID que proporciona Twitter o Facebook
	photo			 : String, // Avatar o foto del usuario
	createdAt	 : {type: Date, default: Date.now} // Fecha de creación



});

// mongoosePaginate usage
userSchema.plugin(mongoosePaginate);

// Export Schema
module.exports = mongoose.model('User', userSchema);

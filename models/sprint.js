const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

// Schema definition
const sprintSchema = Schema({
  sprintName: String,
  duration: {
    startDate: Date,
    endDate: Date
  },
  goal: String,
  remainingHours: Number,
  feedback: String,
  status: {
    type: String,
    enum: ['Active', 'Closed']
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: 'Project'
  }
});

// mongoosePaginate usage
sprintSchema.plugin(mongoosePaginate);

// Export Schema
module.exports = mongoose.model('Sprint', sprintSchema);
